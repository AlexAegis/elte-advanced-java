# Programozási nyelvek (Java), első vizsga 2015.05.29.

# Általános tudnivalók

http://nboldi.web.elte.hu/haladojava/feladat.html

Ebben az ismertetésben az osztályok, valamint a minimálisan szükséges metódusok leírásai fognak szerepelni. A feladatmegoldás során fontos betartani az elnevezésekre és típusokra vonatkozó megszorításokat. Segédfüggvények létrehozhatóak, a feladatban nem megkötött adattagok és elnevezéseik is a feladat megoldójára vannak bízva. Törekedjünk arra, hogy az osztályok belső reprezentációját a lehető legjobban védjük, tehát csak akkor engedjünk, és csak olyan hozzáférést, amelyre a feladat felszólít, vagy amit azt osztályt használó kódrészlet megkíván!

A beadott megoldásodnak működnie kell a mellékelt tesztprogramokkal, de ez nem elégséges feltétele az elfogadásnak. Törekedjünk, hogy a megírt forráskód kellően általános és újrafelhasználható legyen!

Használható segédanyagok: [Java dokumentáció](https://bead.inf.elte.hu/files/java/api/), legfeljebb egy üres lap és toll. Ha bármilyen kérdés, észrevétel felmerül, azt a felügyelőknek kell jelezni, _NEM_ a diáktársaknak!

# A feladat összefoglaló leírása

Készítsünk aknakereső játékot! Az aknakereső játék négyzethálós pályán játszódik, kétféle cella van benne, üres és aknát tartalmazó. A játékos kétféle akciót végezhet, felfedheti egy mező tartalmát vagy megjelölheti a cellát mint aknát tartalmazót. Ha aknát fed fel, akkor a játéknak vége. Ha üres területet, akkor látni fogja, hogy a cella szomszédai közül hány tartalmaz aknát. A játékos akkor nyer, ha minden üres cella fel van fedve és minden aknát tartalmazó meg van jelölve.

A programhoz tartozik egy [grafikus osztály](http://nboldi.web.elte.hu/haladojava/MinesweeperGUI.java), amelynek futtatásával kipróbálható az eredmény. Tartoznak továbbá hozzá [egységtesztek](Tests.java), illetve ehhez tartozó segédosztályok is ([DummyCell.java](DummyCell.java), [CheaterMinesweeper.java](CheaterMinesweeper.java)), amik az egyes osztályok funkcionalitását tesztelik.

# A feladat részletes ismertetése

## Point _(12 pont)_

Valósítsuk meg a `minesweeper.utils.Point` osztályt, amelynek az a feladata, hogy ábrázolja a feladatban kezelendő cellák pozícióját.

*   Benne el kell tudnunk tárolni a privát egész értékű `x` és `y` koordinátákat. Konstruktorában ezeket az `x` és `y` értékeket kapja meg.

*   Legyen egy osztályszintű `ORIGO` nevű pont objektum, ami a `(0,0)` pontot rögzíti.

*   Definiáljuk újra az `Object`-től örökölt `toString` metódust. Az eredmény legyen `(x,y)` formátumú. Például: `(1,2)`. _(3 pont)_

*   Definiáljuk újra az `Object`-től örökölt `equals` metódust. Két `Point` objektum legyen egyenlő, ha az `x` és `y` koordinátáik megegyeznek. _(2 pont)_

*   Definiáljuk újra az `Object`-től örökölt `hashCode` metódust. Azonos koordinátájú pontokra a `hashCode` adjon azonos értéket. _(1 pont)_

*   Valósítsük meg a `Point` osztállyal a `Comparable<Point>` interfészt, hogy a pontok összehasonlíthatók legyenek. Az összehasonlítás történjen először az `x` majd az `y` koordináta mentén. Tehát `(1,2)` kisebbnek minősül, mint `(2,1)`. _(2 pont)_

*   Legyen egy `insideRange` függvénye, amely két másik pontot kap és megmondja, hogy az aktuális pont benne van-e a két pont által kijelölt téglalapban. Feltételezhetjük, hogy az első paraméter bal alsó, a második pedig a jobb fölső pontja a téglalapnak. Például az `(1,2)` pont benne van a `(0,0)` és `(2,2)` pontok által kijelölt téglalapban, de a `(2,3)` pont már nincs benne. _(2 pont)_

*   Legyen egy `neighbours` függvénye, amely visszaadja a szomszédos pontok listáját. Az átlós pontok is szomszédosnak számítanak. Tehát az `(1,2)` pont szomszédai: `(0,1), (0,2), (0,3), (1,1), (1,3), (2,1), (2,2), (2,3)`. _(2 pont)_

## Cell _(1 pont)_

Valósítsuk meg a `minesweeper.cells.Cell` absztrakt osztályt, amely a játék celláinak közös ősosztálya.

*   Legyen egy védett logikai típusú `marked` adattagja, ami azt jelzi, hogy az adott cellát aknának jelölték.

*   Legyen egy `toggleMark` metódusa, ami a `marked` adattagot igazzá teszi, ha eddig hamis volt, hamissá, ha eddig igaz volt. _(1 pont)_

*   Legyen egy `init` absztrakt metódusa, ami `Cell` objektumok listáját (`List`) kapja meg, és inicializálja az adattagot.

*   Legyen egy `hasMine` absztrakt metódusa, ami igazat ad vissza, ha az adott cella aknát tartalmaz, hamisat, ha nem tartalmaz.

*   Legyen egy `revealCell` absztrakt metódusa, ami felfedi az adott cellát. Visszatérési értékként azt adja vissza, hogy a játékos aknára lépett-e. A felfedett cella `marked` adattagja mindenképp hamis lesz.

*   Legyen egy `winningCell` absztrakt metódusa, ami megadja, hogy az adott cella megfelelő-e a játék megnyeréséhez.

## MineCell _(2 pont)_

Valósítsuk meg a `minesweeper.cells.MineCell` osztályt, amely a `Cell` leszármazottja, és olyan cellát reprezentál, amin akna van.

*   Valósítsuk meg az absztrakt függvényeket a fentebb megadott leírásaik alapján. Az `init` metódusa legyen üres. Egy aknát tartalmazó cella akkor megfelelő a játék megnyeréséhez, ha meg van jelölve.

## ClearCell _(5 pont)_

Valósítsuk meg a `minesweeper.cells.ClearCell` osztályt, amely a `Cell` leszármazottja, és olyan cellát reprezentál, amin nincs akna.

*   Az üres cellának van egy `inspected` privát adattagja, ami azt jelzi, hogy az adott mezőt felfedték-e.

*   Az `init` metódusa tárolja el a kapott listát.

*   Célszerű létrehozni egy privát `numNeighbourMines` függvényt, ami visszaadja, hogy az adott mező szomszédjai között hány bombát tartalmazó mező helyezkedik el.

*   A `revealCell` metódus a `Cell` osztályban leírt követelményeken kívül, amennyiben a cellának nincs aknát tartalmazó szomszédja, akkor minden még nem felfedett szomszédját is felfedi (meghívja rá a `revealCell` metódust). Ez a hatás tetszőlegesen messzire tud terjedni.

*   Valósítsuk meg a megmaradó absztrakt függvényeket a fentebb megadott leírásaik alapján. Egy aknát nem tartozó cella akkor megfelelő a játék megnyeréséhez, ha fel van fedve.

## Displayable _(4 pont)_

Hozzuk létre a `minesweeper.utils.Displayable` interface-t, amely azt jelzi, hogy a játék egy eleme szövegesen megjeleníthető.

*   Az interface-nek legyen egy `Display` függvénye, ami a játék adott elemének szöveges megjelenítését adja vissza.

*   A `Cell` osztálynak legyen egy absztrakt `displayNotMarked` függvénye, ami a `dispay`-hez hasonlóan egy szöveget állít elő, de csak akkor lesz meghívva, ha a mező nincs megjelölve.

*   A `Cell` osztály valósítsa meg a `Displayable`-t. A `display` függvény térjen vissza az `"X"` szöveggel, ha a mező meg van jelölve aknaként, különben hívja meg a `displayNotMarked` függvényt. _(1 pont)_

*   A `MineCell` `displayNotMarked` függvénye üres szöveget adjon vissza. A `ClearCell` `displayNotMarked` függvénye üres szöveget adjon vissza, ha a mező még nincs felfedve. Ha már fel van fedve, akkor azt adja vissza, hogy hány akna található a szomszédos mezőkön. Például, ha három akna van a szomszédos mezőköz, akkor a `"3"` szöveget. _(3 pont)_

## NegativeBoardSizeException _(1 pont)_

Hozzunk létre egy saját kivételosztályt, amivel azt fogjuk jelölni, hogy a tábla méretének nem pozitív értéket adtak meg. Ez származzon az `IllegalArgumentException` kivételosztályból. Legyen egy paramétert nem váró konstruktora és hívja meg az ősének üzenetszöveget váró konstruktorát. A megadott üzenet legyen `"Board size cannot be negative"`. _(1 pont)_.

## Minesweeper _(13 pont)_

Végül írjuk meg a `minesweeper.Minesweeper` osztályt, ami majd magát a játékot valósítja meg.

*   Az osztály rendelkezik `width` és `height` egész értékű adattagokkal, amik a szélességét és magasságát rögzítik, egy 0 és 1 közötti értékű `mineRatio` adattaggal, és egy `Random` típusú véletlenszám-generátorral . Legyen továbbá egy védett `cells` tetszőleges megvalósítású, `Map` típusú adattagja, ami `Point` objektumokhoz `Cell` objektumokat rendel.

*   Legyen egy privát `randomCell` függvénye, amely az osztály véletlenszám-generátorát használva az akna aránynak megfelelő eséllyel hoz létre `MineCell` vagy `ClearCell` objektumot.

*   Legyen egy privát `buildCells` függvénye, amely feltölti a `cells` adattagot úgy, hogy a pályaméreten belül levő minden ponthoz a `randomCell` függvénnyel létrehoz egy objektumot és ezt rendeli hozzá.

*   Legyen egy privát `initCells` függvénye, amely minden cellát inicializál, úgy hogy meghívja az `init` függvényét a szomszédos cellák listáját átadva neki. Bátran használjuk a `Point` osztály `neighbours` függvényét, erre a célra írtuk. Ne felejtsük ellenőrizni, hogy a szomszédok a pályán belülre esnek-e.

*   Legyen egy konstruktora, amely egész értékű szélesség és magasság értékeket, egy 0 és 1 közé eső akna arányt, és egy véletlenszám-generátort kap, amiket eltárol. Ha a szélesség vagy a magasság nem pozitív, akkor egy `NegativeBoardSizeException` kivételt vált ki. Meghívja a `buildCells` és `initCells` függvényeket. _(6 pont)_

*   Legyen egy kényelmi konstruktora, amelynek nem kell a véletlenszám-generátort paraméterként átadni, hanem létrehoz egy újat és ezzel hívja meg a másik konstruktort. _(1 pont)_

*   Legyen egy `isWin` függvénye, amely azt adja vissza, hogy megnyertük-e a játékot, vagyis a játék minden cellája nyerő cella-e. _(2 pont)_

*   Legyen egy `displayCell` függvénye, amely egy `Point` típusú paramétert kap és visszaadja az adott ponthoz tartozó cella szöveges megjelenítését. _(1 pont)_

*   Legyen két statikus egészt típusú osztályszintű konstansa, amik az egyes parancsokat rögzítik. A `MARK_CELL` parancs hatására a cellát aknának kell jelölni. A `REVEAL_CELL` felfedi az adott cellát. _(1 pont)_

*   Legyen egy `perform` metódusa, ami a paraméterként megadott ponthoz tartozó cellán elvégzi a paraméterként megadott műveletet. Igaz értékkel tér vissza, ha a lépés hatására elvesztettük a játékot, egyébként hamissal. _(2 pont)_

# Pontozás

A tesztelő által adott pontszám csak becslésnek tekinthető, a gyakorlatvezető levonhat pontokat, vagy adhat részpontokat.

0 - 12: elégtelen (1) 13 - 19 : elégséges (2) 20 - 25 : közepes (3) 26 - 32 : jó (4) 33 - 38 : jeles (5)

Jó munkát! :-)