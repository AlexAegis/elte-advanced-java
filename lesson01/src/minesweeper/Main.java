package minesweeper;

import minesweeper.utils.Point;

public class Main {
    public static void main(String[] args) {
        Point point = new Point(6, 6);
        Point pointa = new Point(-1, -1);
        Point pointb = new Point(10, 10);

        System.out.println(point.insideRange(pointa, pointb));
        point.neighbours().forEach(System.out::println);

    }
}
