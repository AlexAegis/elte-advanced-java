package minesweeper;

import minesweeper.cells.Cell;
import minesweeper.cells.ClearCell;
import minesweeper.cells.MineCell;
import minesweeper.utils.Point;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MineSweeper {

    private int width;
    private int height;

    private int mineRatio; // [0-1]

    private Random random = new Random();

    private Map<Point, Cell> board = new HashMap<>();

    private Cell randomCell() {
        return random.nextFloat() < mineRatio ? new ClearCell() : new MineCell();
    }
}