package minesweeper;

public class NegativeBoardSizeException extends IllegalArgumentException {
    public NegativeBoardSizeException() {
        super("Board size cannot be negative");
    }
}