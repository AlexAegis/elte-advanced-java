package minesweeper.cells;

import java.util.List;

public abstract class Cell implements Displayable {

    protected boolean marked;

    public void toggleMark() {
        marked = !marked;
    }

    @Override
    public String display() {
        return marked ? "X" : displayNotMarked();
    }

    public abstract boolean hasMine();

    public abstract String displayNotMarked();

    public abstract void init(List<Cell> cells);

    public abstract boolean revealCell();

    public abstract boolean winningCell();
}
