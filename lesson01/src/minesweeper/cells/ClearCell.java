package minesweeper.cells;

import java.util.List;

public class ClearCell extends Cell {

    private boolean inspected;

    public ClearCell() {
        super();
    }

    @Override
    public boolean hasMine() {
        return false;
    }

    @Override
    public String displayNotMarked() { // TODO if inspected give the number of mines neighbouring this cell
        return inspected ? "2" : "";
    }

    @Override
    public void init(List<Cell> cells) {

    }

    @Override
    public boolean revealCell() {
        if(inspected) return false;
        inspected = true;
        //if(neighbouringMines() == 0) {
            //revealall neightbourings
       // }
        return false;
    }

    @Override
    public boolean winningCell() {
        return false;
    }
}
