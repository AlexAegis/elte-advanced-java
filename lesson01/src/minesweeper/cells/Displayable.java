package minesweeper.cells;

public interface Displayable {

    String display();

}