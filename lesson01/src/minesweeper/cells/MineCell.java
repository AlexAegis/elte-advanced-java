package minesweeper.cells;

import java.util.List;

public class MineCell extends Cell {

    public MineCell() {
    }

    @Override
    public boolean hasMine() {
        return true;
    }

    @Override
    public String displayNotMarked() {
        return "";
    }

    @Override
    public void init(List<Cell> cells) {

    }

    @Override
    public boolean revealCell() {
        return marked;
    }

    @Override
    public boolean winningCell() {
        return marked;
    }
}
