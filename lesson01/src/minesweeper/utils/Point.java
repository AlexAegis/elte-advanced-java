package minesweeper.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Point implements Comparable<Point> {
    private int x;
    private int y;

    public static Point ORIGO = new Point(0, 0);

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean insideRange(Point a, Point b) {
        return a.x <= x && b.x >= x && a.y <= y && b.y >= y;
    }

    public List<Point> neighbours() {
        return Arrays.stream(Directions.values()).map(this::step).collect(Collectors.toList());
    }

    private Point step(Directions direction) {
        return new Point(x + direction.getX(), y + direction.getY());
    }

    @Override
    public String toString() {
        // return new StringBuilder().append("(").append(x).append(",").append(y).append(")").toString(); // really?
        return "(" + x + "," + y + ")";
    }

    @Override
    public int compareTo(Point o) {
        return x - o.x == 0 ? y - o.y : x - o.x;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}