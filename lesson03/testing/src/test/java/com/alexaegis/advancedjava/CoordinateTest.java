package com.alexaegis.advancedjava;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoordinateTest {

    private Coordinate origo;

    @Before
    public void setUp() throws Exception {
        origo = new Coordinate(0,0);
    }

    @Test
    public void coordTest() throws Exception {
        assertEquals("", origo.translate(2, 2), new Coordinate(2, 2));
    }
}