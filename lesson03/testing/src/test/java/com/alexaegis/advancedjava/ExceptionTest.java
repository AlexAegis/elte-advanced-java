package com.alexaegis.advancedjava;

import org.junit.Test;

public class ExceptionTest {

    @Test(expected = NullPointerException.class)
    public void exceptionTest() throws Exception {
        throw new NullPointerException();
    }
}