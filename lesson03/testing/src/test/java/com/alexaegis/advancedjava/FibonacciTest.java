package com.alexaegis.advancedjava;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciTest {

    @Test
    public void fibonacciZeroTest() {
        assertEquals("Fibonacci zero: f(0)", Fibonacci.fibonacci(0), 0);
    }

    @Test
    public void fibonacciOneTest() {
        assertEquals("Fibonacci one: f(1)", Fibonacci.fibonacci(1), 1);
    }

    @Test
    public void fibonacciTwoTest() {
        assertEquals("Fibonacci two: f(2)", Fibonacci.fibonacci(2), 1);
    }

    @Test
    public void fibonacciThreeTest() {
        assertEquals("Fibonacci two: f(3)", Fibonacci.fibonacci(3), 2);
    }

    @Test
    public void fibonacciFourTest() {
        assertEquals("Fibonacci two: f(4)", Fibonacci.fibonacci(4), 3);
    }
}
