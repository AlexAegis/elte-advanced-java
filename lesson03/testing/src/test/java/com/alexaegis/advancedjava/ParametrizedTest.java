package com.alexaegis.advancedjava;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(value = Parameterized.class)
public class ParametrizedTest {

    private int number;
    private int expected;

    public ParametrizedTest(int number, int expected) {
        this.number = number;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index}: fibonacci({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 0},
                {1, 1},
                {2, 1},
                {3, 2},
                {4, 3},
                {5, 5},
                {6, 8},
                {7, 13},
                {8, 21},
                {9, 34},
                {10, 55},
                {11, 89},
                {12, 144}
        });
    }

    @Test
    public void fibonacciBatch() throws Exception {
        assertEquals(Fibonacci.fibonacci(number), expected);
    }

}