###Függőség-kezelés (Maven), viselkedés-alapú tesztelés (Mockito)

1. Készíts három osztályt: a Posta osztály kezdetben két Postafiok 
példányt kap meg. A Posta osztály valogat metódusa egy Level példányt 
kap, és kézbesíti: a páros címűeket az első, a páratlan 
címűeket a második postafiókba (fogad metódus). A tesztelő 
küldjön el néhány levelet, és ellenőrizze, hogy a megfelelő helyre érkeznek-e.
2. Az előző tesztelőt alakítsd át viselkedési tesztelővé, hogy 
a Posta osztályt önmagában, a Postafiok használata nélkül tesztelje. 
    - Készíts két Postafiok típusú mock-ot és ellenőrizd, hogy a 
    megfelelő leveleket kapják meg. Mockito
    - Kényszerítsd ki, hogy a megfelelő sorrendben hívódjanak meg a
     fogad metódusok. InOrder
3. Készíts egy FileSystem interfészt, két metódussal: appendFile és readFile. 
Mindkettő megkapja a fájl elérési útját (String) ezen kívül az 
appendFile megkapja a szöveget amit a fájl végére kell fűzni.
4. Készíts egy RealFileSystem osztályt, ahol a FileSystem műveleteit megvalósítod.
5. Készíts egy PersistentStack osztályt, ami tartalmaz egy memóriabeli 
Stringekből álló vermet és ennek a tartalmát írja mindig ki fájlba. 
A konstruktorban kapja meg a fájlrendszert és a fájl nevét. Legyen egy load művelete, 
ami betölti a fájl tartalmát, fölülírva a memóriában levő adatokat. Legyen egy push 
metódusa ami új elemet ad a memóriában és a fájlban is.
6. Teszteljük ezt az osztályt.
    - A push függvény teszteléséhez használjunk viselkedés-alapú tesztelőt.
    - A load függvény teszteléséhez adjuk meg a fájlrendszer objektum működését, 
    amennyiben a kiválasztott fájlt olvasná egy objektum.