package com.alexaegis.advancedjava.filesystem;

public interface FileSystem {

    void appendFile();

    void readFile();
}