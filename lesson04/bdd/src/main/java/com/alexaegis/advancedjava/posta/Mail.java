package com.alexaegis.advancedjava.posta;

public class Mail {

    private int address;

    public Mail(int address) {
        this.address = address;
    }

    public int getAddress() {
        return address;
    }
}
