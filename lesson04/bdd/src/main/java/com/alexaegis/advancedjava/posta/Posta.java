package com.alexaegis.advancedjava.posta;

public class Posta {

    private Postafiok p1;
    private Postafiok p2;

    public Posta(Postafiok p1, Postafiok p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void valogat(Mail mail) {
        if(mail.getAddress() % 2 == 0) {
            p1.fogad(mail);
        } else {
            p2.fogad(mail);
        }
    }
}
