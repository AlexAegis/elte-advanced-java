package com.alexaegis.advancedjava.posta;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PostaInOrderTest {

    private Posta posta;
    private Postafiok p1;
    private Postafiok p2;
    private InOrder inOrder;

    @Before
    public void setUp() throws Exception {
        p1 = mock(Postafiok.class);
        p2 = mock(Postafiok.class);
        inOrder = Mockito.inOrder(p1, p2);
        posta = new Posta(p1, p2);
    }

    @Test
    public void sendTest() throws Exception {
        posta.valogat(new Mail(10));
        posta.valogat(new Mail(11));
        inOrder.verify(p1).fogad(any(Mail.class));
        inOrder.verify(p2).fogad(any(Mail.class));
    }
}