package com.alexaegis.advancedjava.posta;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PostaTest {

    private Posta posta;
    private Postafiok p1;
    private Postafiok p2;

    @Before
    public void setUp() throws Exception {
        p1 = mock(Postafiok.class);
        p2 = mock(Postafiok.class);
        posta = new Posta(p1, p2);
    }

    @Test
    public void sendTest() throws Exception {
        posta.valogat(new Mail(10));
        posta.valogat(new Mail(11));
        verify(p1).fogad(any(Mail.class));
        verify(p2).fogad(any(Mail.class));
    }
}